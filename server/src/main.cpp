#include "caf/actor_config.hpp"
#include "caf/actor_system_config.hpp"
#include "caf/behavior.hpp"
#include "caf/exit_reason.hpp"
#include "caf/io/connection_handle.hpp"
#include "caf/io/middleman.hpp"
#include "caf/io/receive_policy.hpp"
#include "caf/io/system_messages.hpp"
#include "caf/system_messages.hpp"
#include <caf/all.hpp>
#include <caf/io/all.hpp>

caf::behavior connection_worker(caf::io::broker* self, caf::io::connection_handle hdl) {
  self -> configure_read(hdl, caf::io::receive_policy::at_most(1024));
  self -> send(self, "Hello, I'm the server!");
  return {
    [=](const caf::io::new_data_msg& msg) {
      auto pos = msg.buf.data();
      std::string s = std::string{0};
      memcpy(&s, pos, 24);

      aout(self) << "Worker received: " << s << std::endl;
    },
    [=](const caf::io::connection_closed_msg&) {
      aout(self) << "Connection closed" << std::endl;
      self -> quit();
    },
    [=](const std::string& s) {
      self -> write(hdl, 1024, &s);
      self -> flush(hdl);
    }
  };
}

caf::behavior server(caf::io::broker* self) {
  static int counter = 0;
  aout(self) << "Server is running..." << std::endl;
  self -> set_down_handler([=](caf::down_msg&){
      --counter;
  });

  return caf::behavior {
    [=] (const caf::io::new_connection_msg& msg) {
      ++counter;
      aout(self) << "Server accepted a new connection, current number of connections: " << counter << std::endl;
      auto worker = self -> fork(connection_worker, msg.handle);
      self -> monitor(worker);
      self -> link_to(worker);
    }
  };
}

class config : public caf::actor_system_config {
  public:
    int port = 8080;
    std::string host = "127.0.0.1";
    bool server_mode = true;

    config() {
    opt_group{custom_options_, "global"}
      .add(port, "port,p", "set port")
      .add(server_mode, "server-mode,s", "enable server mode");
  }
};

void caf_main(caf::actor_system& system, const config& cfg) {
  std::cout << "Starting server" << std::endl;
  auto server_actor = system.middleman().spawn_server(server, cfg.port);

  if (!server_actor) {
    std::cerr << "Failed to spawn server: " << to_string(server_actor.error()) << std::endl;
    return;
  }
}

CAF_MAIN(caf::io::middleman)
