#include "caf/io/receive_policy.hpp"
#include "caf/send.hpp"
#include <caf/all.hpp>
#include <caf/io/middleman.hpp>

template <size_t Size>
constexpr size_t cstr_size(const char (&)[Size]) {
  return Size;
}

caf::behavior broker_impl(caf::io::broker* self, caf::io::connection_handle hdl) {
  assert(self -> num_connections() == 1);
  self -> configure_read(hdl, caf::io::receive_policy::at_most(1024));
  self -> send(self, "Hi, I'm the client!");

  return caf::behavior {
    [=](const caf::io::connection_closed_msg& msg) {
      if(msg.handle == hdl) {
        aout(self) << "Connection closed" << std::endl;
        self -> quit(caf::exit_reason::remote_link_unreachable);
      }
    },
    [=](std::string& s) {
      self -> write(hdl, 1024, &s);
      self -> flush(hdl);
    },
    [=](const caf::io::new_data_msg& msg) {
      auto pos = msg.buf.data();
      std::string s = std::string{0};
      memcpy(&s, pos, 24);

      aout(self) << "Received: " << s << std::endl;
    }
  };
}

class config : public caf::actor_system_config {
public:
  uint16_t port = 8080;
  std::string host = "127.0.0.1";
  bool server_mode = false;

  config() {
    opt_group{custom_options_, "global"}
      .add(port, "port,p", "set port")
      .add(host, "host,H", "localhost")
      .add(server_mode, "server-mode,s", "enable server mode");
  }
};

void caf_main(caf::actor_system& system, const config& cfg) {
  auto io_actor = system.middleman().spawn_client(broker_impl, cfg.host, cfg.port);
  
  if (!io_actor) {
    std::cerr << "failed to spawn client: " << to_string(io_actor.error()) << std::endl;
    return;
  }

}

CAF_MAIN(caf::io::middleman)
